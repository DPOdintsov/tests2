﻿Option Explicit


Dim objFileSystem, objShell, objReadingFile, objXMLFile, xmlParserC
Dim strDesktop, strLine, arrOneLine
Dim objBuyers
Dim arrInterval, strInterval, strArrBuyerToXML, rootNodeC
Dim i

Const FOR_READING = 1

Set objBuyers = NewBuyerList

Set objFileSystem = CreateObject("Scripting.FileSystemObject")				'создание объекта для работы с файловой системой
Set objShell = CreateObject("WScript.Shell")							'создание объекта для работы с переменными окружения
strDesktop = objShell.ExpandEnvironmentStrings("%USERPROFILE%") & "\Desktop\"			'получаем значение переменной, указывающей на каталог профиля текущего пользователя


'Загрузка данных из текстового файла
'--------------------------------------------------------------------------------------------------------------------------------------------------
If objFileSystem.FileExists(strDesktop & "buyers.txt") Then						'Проверка файла на существование
	Set objReadingFile = objFileSystem.OpenTextFile("" & strDesktop & "buyers.txt", FOR_READING, False, 0)		'если файл есть, открываем его на чтение
Else
	MsgBox("Указанный файл не существует. Скопируйте его на рабочий стол")							'если нет, то выводим сообщение и выходим из процедуры
End If

Do While Not objReadingFile.AtEndOfStream		'цикл по строкам файла с данными
	strLine = objReadingFile.ReadLine			'читаем строку в цикле до конца файла
	arrOneLine = Split(strLine, ";", -1, 1)		'разбиваем строку по разделителю на два элемента

	objBuyers.SetBuyer = arrOneLine					'Вызов метода сохранения данных в свойствах класса Buyer
	
	Call objBuyers.AddEntry(objBuyers.GetBuyerRecord())				'вызов метода добавления данных в массив BuyerList
Loop
'--------------------------------------------------------------------------------------------------------------------------------------------------


'Выбор записи из добавленных по номеру банковского счёта
Call objBuyers.DisplayEntry(objBuyers.GetBankFromUser)								'вызов метода поиска данных по введённому номеру банковского счёта


'Выгрузка данных в файл XML
'--------------------------------------------------------------------------------------------------------------------------------------------------
Set objXMLFile = NewXMLFile
Set xmlParserC = objXMLFile.xmlParserGet()

xmlParserC.appendChild(xmlParserC.createProcessingInstruction("xml", "version='1.0' encoding='windows-1251'"))	'добавление начальных параметров файла
Set rootNodeC = xmlParserC.appendChild(xmlParserC.createElement("BUYERLIST"))				'установка коревого элемента

For i = 1 To objBuyers.BuyersCount Step 1
	strArrBuyerToXML = objBuyers.GetEntry("" & i)
	Call objXMLFile.UploadToXML(strArrBuyerToXML, rootNodeC)
Next
xmlParserC.save(strDesktop & "buyers.xml")										'сохранение данных в файл
'--------------------------------------------------------------------------------------------------------------------------------------------------


'Вывод отсортированного списка
Call objXMLFile.GetNamesInABC														'вызов процедуры вывода списка покупателей с сортировкой по алфавиту

'--------------------------------------------------------------------------------------------------------------------------------------------------


'Ввод интервала номеров кредитных карт и отображение записей, входящих в этот интервал
strInterval = InputBox("Введите интервал выбираемых значений кредитных карт в виде (Х-Х)", "Выбор записей")	'ввод интервала номеров кредитных карт
arrInterval = Split(strInterval, "-", -1, 1)						'преобразование интервала в массив

If IsNumeric(arrInterval(0)) And IsNumeric(arrInterval(1)) Then		'проверка введённых данных
	Call objXMLFile.GetNamesByCardNumber(arrInterval(0), arrInterval(1))				'и вызов фукнции поиска и отображения данных из указанного интервала
End If
