﻿Option Explicit

Dim objFileSystem, objShell, objFile
Dim i
Dim strArrTemp
Dim strPathToDesktopFolder

Set objFileSystem = CreateObject("Scripting.FileSystemObject")				'создание объекта для работы с файловой системой
Set objShell = CreateObject("WScript.Shell")							'создание объекта для работы с системными переменными
strPathToDesktopFolder = objShell.ExpandEnvironmentStrings("%USERPROFILE%") & "\Desktop\"		'получаем путь до "рабочего стола"

strArrTemp = LoadPasswords(strPathToDesktopFolder & "pwd.txt", objFileSystem)				'вызываем функцию загрузки паролей из текстого файла в массив
strArrTemp = ArraySort(strArrTemp)										'вызываем фукнцию сортировки загруженного массива паролей

Set objFile = objFileSystem.CreateTextFile(strPathToDesktopFolder & "testfile.txt", True)	'создаём текстовый файл для записи результатов работы
For i = UBound(strArrTemp) To UBound(strArrTemp) - 9 Step -1	'цикл по элементам массива
	objFile.WriteLine(strArrTemp(i, 0) & " - " & strArrTemp(i, 1))		'записываем в созданный файл i-ое значение массива (пароль - кол-во повторений данного пароля)
Next
objFile.Close														'закрываем файл
Set objFileSystem = Nothing													'уничтожаем созданные объекты
Set objShell = Nothing
