﻿Option Explicit

Dim intOperationSelection
Dim strSearchString
Dim NewLibrary
Dim intSortBy

Dim strInputAuthor, strInputCaption, strInputYear

Set NewLibrary = InitLibrary						'инициализация экземпляра класса

'Вывод сообщения с выбором выполняемой операции
Do
	Do
		intOperationSelection = InputBox("Выберите операцию" & vbLf & "1 - Поиск," & vbLf & "2 - Добавление," & vbLf & "3 - Удаление," & vbLf & "4 - Сортировка," & vbLf & "0 - Выход ", "Выбор операции")
	Loop While Not (intOperationSelection = 1 Or intOperationSelection = 2 Or intOperationSelection = 3 Or intOperationSelection = 4 Or intOperationSelection = 0)
	
	Select Case intOperationSelection					
		Case 1									'Выбран Поиск по библиотеке
			strSearchString = InputBox("Введите год в виде YYYY или автора произведения", "Вариант поиска")		'Ввод данных для поиска - год или автор
			Call NewLibrary.Search(strSearchString)			'вызов метода поиска класса 
		Case 2									'выбрано Добавление в библиотеку
			NewLibrary.Author = InputBox("Укажите автора", "Добавление книги")				'ввод автора произведения
			NewLibrary.BookName = InputBox("Укажите название книги", "Добавление книги")	'ввод названия книги
			NewLibrary.Year = InputBox("Укажите год книги", "Добавление книги")				'ввод года издания книги
			Call NewLibrary.AddNewBook															'вызов метода сохранения данных в свойствах класса
		Case 3
			Call NewLibrary.DeleteBook															'Вызов метода класса для удаления существующей записи
		Case 4														'Выбран вариант Сортировка
			Do														'ввод варианта сортировки
				intSortBy = InputBox("Укажите номер поля для сортировки" & vbLf & "1 - Автор," & vbLf & "2 - Название книги," & vbLf & "3 - Год издания", "Выбор операции")
			Loop While Not (intSortBy = 1 Or intSortBy = 2 Or intSortBy = 3)
			Call NewLibrary.SortBy(intSortBy)								'вызов метода класса сортировки данных
	End Select
Loop While Not intOperationSelection = 0

