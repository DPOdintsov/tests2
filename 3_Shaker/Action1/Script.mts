﻿Option Explicit

Dim intPower, i, j, k, tempValue
Dim arrSource()
Dim arrDestination()
Dim intNewIndex
Dim strTemp

Do																							'Ввод количества элементов массива
	intPower = InputBox("Введите количество элементов массива", "Пересортировка массива")
Loop While Not IsNumeric(intPower)

intPower = CInt(intPower) - 1										'Преобразование к целому

Redim arrSource(intPower)										'Ресайз исходного массива
Redim arrDestination(intPower)									'Ресайз целевого массива

For i = 0 To intPower											'цикл по элементам массива							
	arrSource(i) = RandomNumber(0, 100)						'получение случайного числа и его сохранение в массиве
Next

strTemp = "Исходный массив: "								'инициализация строковой переменной для итогового сообщения
For i = 0 To intPower											'копирование элементов массива для наглядного отображения в конце работы
	strTemp = strTemp & arrSource(i) & " "
Next

For k = 1 To RandomNumber(1, intPower + 1) Step 1 'цикл сдвига элементов массива на рандомное кол-во элементов больше 0 но меньше размера массива минус один
	For i = 0 To intPower					'цикл по элементам массива
		j = i								'дублируем счётчик
		If i = intPower Then				'если конечный элемент, то записываем его в массив с индексом 0
			i = -1							'
		End If
		arrDestination(j) = arrSource(i + 1)	'сдвигаем элементы массива
		I = J								'восстанавливаем счётчик
	Next
Next

strTemp = strTemp & vbLf & "Перемеш. массив: "				'копирование перемешанного массива в итоговую строку
For i = 0 To intPower
	strTemp = strTemp & arrDestination(i) & " "
Next

MsgBox strTemp													'отображение итогов работы
