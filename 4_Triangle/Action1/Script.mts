﻿Option Explicit

Dim objTriangle
Dim checkSides
Dim lngArrMed
Dim strSides
Dim intArrSides
Dim strArrCoordsA, strArrCoordsB, strArrCoordsC
Dim intCoords

Set objTriangle = NewTriangle							'Инициирование класса

strSides = InputBox("Введите стороны треугольника в виде X,Y,Z", "Стороны треугольника") 'ввод размера сторон треугольника
strSides = Split(strSides, ",", -1, 1)														'разбивка введёной строки
intArrSides = Array(CInt(strSides(0)),CInt(strSides(1)),CInt(strSides(2)))						'преобразование в числа
objTriangle.SetTriangleA = intArrSides(0)
objTriangle.SetTriangleB = intArrSides(1)
objTriangle.SetTriangleC = intArrSides(2)

MsgBox "Saved sides is: " & objTriangle.GetSideA & "-" & objTriangle.GetSideB & "-" & objTriangle.GetSideC
MsgBox "Triangle S is: " & objTriangle.TriangleS														'вызов функции расчёта площади треугольника по указанным сторонам
MsgBox "Triangle perimetr is: " & objTriangle.TrianglePerimetr										'вызов фукнции расчёта периметра треугольника

strArrCoordsA = InputBox("Введите координаты вершины А треугольника в виде X1,Y1", "Координаты вершин треугольника") 	'ввод координат вершин треугольника
strArrCoordsA = Split(strArrCoordsA, ",", -1, 1)																'разбивка введёной строки на параметры
strArrCoordsB = InputBox("Введите координаты вершины В треугольника в виде X2,Y2", "Координаты вершин треугольника") 	'ввод координат вершин треугольника
strArrCoordsB = Split(strArrCoordsB, ",", -1, 1)																'разбивка введёной строки на параметры
strArrCoordsC = InputBox("Введите координаты вершины С треугольника в виде X3,Y3", "Координаты вершин треугольника") 	'ввод координат вершин треугольника
strArrCoordsC = Split(strArrCoordsC, ",", -1, 1)																'разбивка введёной строки на параметры
intArrSides = Array(CInt(strArrCoordsA(0)),CInt(strArrCoordsA(1)),CInt(strArrCoordsB(0)),CInt(strArrCoordsB(1)),CInt(strArrCoordsC(0)),CInt(strArrCoordsC(1)))	'преобразование в числа
lngArrMed = objTriangle.TriangleMedians(Array(intArrSides(0),intArrSides(1)), Array(intArrSides(2),intArrSides(3)), Array(intArrSides(4),intArrSides(5)))	'вызов функции расчёта координат пересечения медиан
MsgBox "Triangle median point is in: " & lngArrMed(0) & ";" & lngArrMed(1)								'вывод результата
