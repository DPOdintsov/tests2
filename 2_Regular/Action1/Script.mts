﻿Option Explicit

Dim intVariant		'ввод варианта обработки данных 

Dim startTime, endTime
Dim strSource
'Выбор варианта обработки строки
Do
	intVariant = InputBox("Выберите метод (1 - RegExp, 2 - simple) ", "Замена адресов")
Loop While Not (intVariant = 1 Or intVariant = 2)

strSource = InputBox("Input web or email address", "Regular")					'ввод строки данных для обработки
strSource = Trim(strSource)					'Удаление лишних пробелов в начале и в конце строки

' Обработка строки в зависимости от выбранного варианта
Select Case intVariant
	Case 1
		MsgBox "Исходная строка: " & strSource & vbLf & "Преобразованная строка: " & RegExpString(strSource, startTime, endTime)	'Вызов функции обработки RegExp
	Case 2
		MsgBox "Исходная строка: " & strSource & vbLf & "Преобразованная строка: " & MakeItSimple(strSource, startTime, endTime)	'Вызов функции обработки обычным методом
End Select
MsgBox "Execution time is: " & startTime - endTime
